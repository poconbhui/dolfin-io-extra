Dolfin Conversion
-----------------

This is a set of scripts for converting to/from Dolfin compatible formats.
Particularly Dolfin XML formats used for meshes, mesh functions and functions.

So far, Tecplot and Patran files are supported.
