#!/usr/bin/env python

from distutils.core import setup

setup(
    name = "dolfin-io-extra",
    version = "0.1",
    packages = ["dolfin_io_extra"]
)
