#!/usr/bin/env python

from dolfin_io_extra import PatranFile

import dolfin

tmp_filename = "tmp_file.dat"

print "Testing Mesh Reading/Writing..."

# Generate a standard mesh
cube_mesh = dolfin.UnitCubeMesh(5,5,5)

# Write the mesh to a patran file
patran_file_write = PatranFile(tmp_filename)
patran_file_write << cube_mesh

# Read the mesh from a patran file
patran_file_read = PatranFile(tmp_filename)
test_mesh = dolfin.Mesh()
patran_file_read >> test_mesh

# Test all the vertices are the same
vertex_tol = 1e-6
for v1, v2 in zip(dolfin.vertices(cube_mesh), dolfin.vertices(test_mesh)):
    if v1.point().squared_distance(v2.point()) > vertex_tol:
        raise Exception(
            "Original and read points too far apart!"
        )

# Test all the connectivities are the same
for c1, c2 in zip(dolfin.cells(cube_mesh), dolfin.cells(test_mesh)):
    for e1, e2 in zip(c1.entities(0), c2.entities(0)):
        if e1 != e2:
            raise Exception(
                "Original and read elements don't have the same ids!"
            )


print "Testing MeshFunction reading/writing..."

cube_mesh = dolfin.UnitCubeMesh(5,5,5)
mesh_function = dolfin.MeshFunction('size_t', cube_mesh, 3)
for i, cell in enumerate(dolfin.cells(cube_mesh)):
    mesh_function[cell] = i*27

PatranFile(tmp_filename) << mesh_function

test_mesh_function = dolfin.MeshFunction('size_t', cube_mesh, 3)

PatranFile(tmp_filename) >> test_mesh_function

for i, cell in enumerate(dolfin.cells(cube_mesh)):
    if mesh_function[cell] != test_mesh_function[cell]:
        raise Exception(
            "Original and read MeshFunctions don't have same cell values."
        )

print "PatranFile Test Passed!"
