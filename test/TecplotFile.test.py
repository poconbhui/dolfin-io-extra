#!/usr/bin/env python

from dolfin_io_extra import TecplotFile

import dolfin

test_file = "tmp_file.dat"


print "Testing mesh reading/writing..."

cube_mesh = dolfin.UnitCubeMesh(5,5,5)

TecplotFile(test_file) << cube_mesh

test_mesh = dolfin.Mesh()
TecplotFile(test_file) >> test_mesh

# Check vertices are the same
vertex_tol = 1e-6
for v1, v2 in zip(dolfin.vertices(cube_mesh), dolfin.vertices(test_mesh)):
    if v1.point().squared_distance(v2.point()) > vertex_tol**2:
        raise Exception(
            "Error: Vertices too far apart!"
        )

for c1, c2 in zip(dolfin.cells(cube_mesh), dolfin.cells(test_mesh)):
    for e1, e2 in zip(c1.entities(0), c2.entities(0)):
        if e1 != e2:
            raise Exception(
                "Error: Entities not the same!"
            )

print "TecplotFile Test Passed!"
