#!/usr/bin/env python

import dolfin
import re
import numpy

class PatranFile:

    def __init__(self, filename):
        self._filename = filename

    def write(self, towrite):
        stream = open(self._filename, 'w')

        if isinstance(towrite, dolfin.Mesh):
            self.write_mesh_to_stream(towrite, stream)
        elif isinstance(towrite, dolfin.MeshFunctionSizet):
            self.write_mesh_function_to_stream(towrite, stream)
        else:
            raise NotImplementedError(
                "Writing " + str(towrite) + " is not supported."
            )

    def __lshift__(self, towrite):
        self.write(towrite)

    def read(self, toread):
        stream = open(self._filename, 'r')

        if isinstance(toread, dolfin.Mesh):
            self.read_mesh_from_stream(toread, stream)
        elif isinstance(toread, dolfin.MeshFunctionSizet):
            if not toread.dim() == 3:
                raise NotImplementedError(
                    "Reading to MeshFunction with dim != 3 is not supported"
                )

            self.read_mesh_function_from_stream(toread, stream)
        else:
            raise NotImplementedError(
                "Reading " + str(towrite) + " is not supported."
            )

    def __rshift__(self, toread):
        self.read(toread)


    @staticmethod
    def write_mesh_function_to_stream(mesh_function, stream):
        mesh = mesh_function.mesh()
        PatranFile.write_mesh_to_stream(
            mesh, stream, element_pids=mesh_function
        )


    @staticmethod
    def write_mesh_to_stream(mesh, stream, element_pids=None):
        r'''
        Write a Dolfin Mesh out to a stream as a
        Patran Neutral File.

        Optional mesh_function to set element_pid values
        '''

        if mesh.topology().dim() != 3:
            raise ValueError("Only 3D mesh conversion supported!")

        if mesh.type().cell_type() != dolfin.CellType.tetrahedron:
            raise ValueError("Only tetrahedral cells supported!")


        #######################################################################
        ## Start Patran file.                                                ##
        #######################################################################
        ## File format taken from                                            ##
        ## http://www.ansys.com/staticassets/ANSYS                           ##
        ##  /Initial%20Content%20Entry/General%20Articles%20-%20Products     ##
        ##  /ICEM%20CFD%20Interfaces/patran.htm                              ##
        ##                                                                   ##
        ## and                                                               ##
        ##     Patran 2014.1 Reference Manual Part 1: Basic Functions,       ##
        ##     The Neutral File, p 892                                       ##
        ## found at                                                          ##
        ##     https://simcompanion.mscsoftware.com/infocenter/              ##
        ##         index?page=content&id=DOC10756                            ##
        ##         &cat=2014.1_PATRAN_DOCS&actp=LIST                         ##
        #######################################################################


        # Common string formats
        i28i8 = "%02d%8d%8d%8d%8d%8d%8d%8d%8d\n"


        #######################################################################
        ## Packet 25: Title Card                                             ##
        #######################################################################
        ## The title card packet contains the following:                     ##
        ##         25 ID IV KC                                               ##
        ##         TITLE                                                     ##
        ##                                                                   ##
        ## where:  ID = 0 (not applicable)                                   ##
        ##         IV = 0 (not applicable                                    ##
        ##         KC = 1                                                    ##
        ##         TITLE = ICEM-PATRAN INTERFACE - Version ... -             ##
        ##                                                                   ##
        ## format: (I2, 8I8)                                                 ##
        ##         (80A4)                                                    ##
        #######################################################################

        stream.write(
            i28i8%(25, 0, 0, 1, 0, 0, 0, 0, 0)
        )
        stream.write("%-80s\n"%("dolfin-conversion PatranFile.py"))



        #######################################################################
        ## Packet 26: Summary Data                                           ##
        #######################################################################
        ## The summary data packet contains the following:                   ##
        ##         26 ID IV KC N1 N2 N3 N4 N5                                ##
        ##         DATE TIME VERSION                                         ##
        ##                                                                   ##
        ## where:  ID = 0 (not applicable)                                   ##
        ##         IV = 0 (not applicable)                                   ##
        ##         KC = 1                                                    ##
        ##         N1 = number of nodes                                      ##
        ##         N2 = number of elements                                   ##
        ##         N3 = number of materials                                  ##
        ##         N4 = number of element properties                         ##
        ##         N5 = number of coordinate frames                          ##
        ##         DATE = dd-mm-yy                                           ##
        ##         TIME = hh:mm:ss                                           ##
        ##         VERSION = ??                                              ##
        ##                                                                   ##
        ## format: (I2, 8I8)                                                 ##
        #######################################################################

        stream.write(
            i28i8%(26, 0, 0, 1, mesh.num_vertices(), mesh.num_cells(), 1, 0, 0)
        )
        stream.write(
            "%-12s%-8s%12s\n"%("00/00/00", "00:00:00", "2.5")
        )



        #######################################################################
        ## Packet 01: Node Data                                              ##
        #######################################################################
        ## The node data packet contains the following:                      ##
        ##         1 ID IV KC                                                ##
        ##         X Y Z                                                     ##
        ##         ICF GTYPE NDF CONFIG CID PSP                              ##
        ##                                                                   ##
        ## where:  ID = node ID                                              ##
        ##         IV = 0 (not applicable)                                   ##
        ##         KC = number of lines in data card = 2                     ##
        ##         X, Y, Z = X, Y, Z cartesian coordinate of the node        ##
        ##         ICF = 1 (referenced)                                      ##
        ##         GTYPE = G                                                 ##
        ##         NDF = 2 or 3 for 2D or 3D model respectively              ##
        ##         CONFIG = 0 (not applicable)                               ##
        ##         CID = 0 i.e. global cartesian coordinate system           ##
        ##         PSPC = 000000 (not used)                                  ##
        ##                                                                   ##
        ## format: (I2, 8I8)                                                 ##
        ##         (3E16.9) (Maybe actually (E15.8, 1X, E15.8, 1X, E15.8)    ##
        ##         (I1, 1A1, I8, I8, I8, 2X, 6I1)                            ##
        #######################################################################

        for i, vertex in enumerate(dolfin.vertices(mesh)):
            if i != vertex.index():
                print "Warning: Vertex output not in order!"

            # Remember, dolfin is 0 indexed, patran is 1 indexed
            stream.write(i28i8%(1, vertex.index()+1, 0, 2, 0, 0, 0, 0, 0))
            stream.write(
                " % 14.8e % 14.8e % 14.8e\n"%(
                    vertex.x(0), vertex.x(1), vertex.x(2)
                )
            )
            stream.write(
                "%1d%-1s%8d%8d%8d  %1d%1d%1d%1d%1d%1d\n"%(
                    0, "G", 3, 0, 0, 0, 0, 0, 0, 0, 0
                )
            )



        #######################################################################
        ## Packet 02: Element Data                                           ##
        #######################################################################
        ## The element data packet contains the following:                   ##
        ##         2 ID IV KC N1 N2                                          ##
        ##         NODES CONFIG PID CEID q1 q2 q3                            ##
        ##         LNODES                                                    ##
        ##         ADATA                                                     ##
        ##                                                                   ##
        ## where:  ID = element ID                                           ##
        ##         IV = shape (2=bar, 3=tri, 4=quad,                         ##
        ##                     5=tet, 7=wedge, 8=hex, 9=pyra)                ##
        ##         KC = number of lines in data card                         ##
        ##         N1 = 0 (not used)                                         ##
        ##         N2 = 0 (not used)                                         ##
        ##         NODES = number of nodes in the element                    ##
        ##         CONFIG = 0 (not used)                                     ##
        ##         PID = element property ID                                 ##
        ##         CEID = 0 (not used)                                       ##
        ##         q1,q2,q3 = 0 (not used)                                   ##
        ##         LNODES = element corner nodes followed by                 ##
        ##                  additional nodes                                 ##
        ##         ADATA : not used                                          ##
        ##                                                                   ##
        ## format: (I2, 8I8)                                                 ##
        ##         (I8, I8, I8, I8, 3E16.9)                                  ##
        ##         (10I8)                                                    ##
        #######################################################################

        for i, cell in enumerate(dolfin.cells(mesh)):
            if i != cell.index():
                print "Warning: Cell output not in order!"

            vertex_ids = []
            vertex_points = []
            for v in dolfin.vertices(cell):
                vertex_ids.append(v.index()+1)
                vertex_points.append(v.point())

            def volume(vertex_points):
                a = vertex_points[0] - vertex_points[3]
                b = vertex_points[1] - vertex_points[3]
                c = vertex_points[2] - vertex_points[3]

                return (
                      a.x()*(b.y()*c.z() - b.z()*c.y())
                    + a.y()*(b.z()*c.x() - b.x()*c.z())
                    + a.z()*(b.x()*c.y() - b.y()*c.x())
                )

            vol = volume(vertex_points)
            if vol < 0:
                v3 = vertex_ids[3]
                vertex_ids[3] = vertex_ids[2]
                vertex_ids[2] = v3

            property_id = 0
            if element_pids:
                property_id = element_pids[i]

            # +1 for Patran 1 indexing.
            stream.write(i28i8%(2, cell.index()+1, 5, 2, 0, 0, 0, 0, 0))
            stream.write(
                "%8d%8d%8d%8d%16.8e%16.8e%16.8e\n"%(
                    4, 0, property_id, 0, 0, 0, 0
                )
            )
            stream.write(
                "%8d%8d%8d%8d\n"%(
                    vertex_ids[0], vertex_ids[1], vertex_ids[2], vertex_ids[3]
                )
            )

        # End of file card
        # For whatever reason, KC=1 in the docs when it should be probs be 0.
        stream.write(i28i8%(99, 0, 0, 1, 0, 0, 0, 0, 0))


    @staticmethod
    def read_mesh_from_stream(mesh, stream):
        parser = PatranNeutralParser()
        parser.parse(stream)

        mesh_editor = dolfin.MeshEditor()
        mesh_editor.open(mesh, 3,3)

        mesh_editor.init_vertices(parser.num_nodes())
        mesh_editor.init_cells(parser.num_elements())

        for i, coord in enumerate(parser.nodes()):
            mesh_editor.add_vertex(i, coord[0], coord[1], coord[2])

        for i, node_ids in enumerate(parser.elements()):
            mesh_editor.add_cell(
                i, node_ids[0], node_ids[1], node_ids[2], node_ids[3]
            )

        # All done!
        mesh_editor.close()


    @staticmethod
    def read_mesh_function_from_stream(mesh_function, stream):
        parser = PatranNeutralParser()
        parser.parse(stream)

        for i, element_pid in enumerate(parser.element_pids()):
            mesh_function[i] = element_pid



class PatranNeutralParser:
    r'''
    Parse a Patran Neutral file for a title, summary data, node data
    and element data.

    Currently only 3D nodes and tetrahedral meshes are supported.

    self.nodes() returns a list of vertex coordinates.
    self.elements() returns a list of elements, where elements are
    a list of 0 indexed ids representing nodes in self.nodes()
    '''

    def __init__(self):
        # Title Card
        self._title = None

        # Summary Data
        self._num_nodes = None
        self._num_elements = None
        self._num_materials = None
        self._num_element_properties = None
        self._num_coordinate_frames = None

        # Node Data
        self._nodes = None

        # Element Data
        self._elements = None
        self._element_pids = None


    def num_nodes(self):
        return self._num_nodes

    def num_elements(self):
        return self._num_elements

    def nodes(self):
        return self._nodes

    def elements(self):
        return self._elements

    def element_pids(self):
        return self._element_pids


    def parse(self, stream):
        r'''
        Parse an input stream representing a Patran Neutral File.
        '''

        # Map between card IDs and parsers
        it_to_parser_dict = {
            25: self.parse_title_card,
            26: self.parse_summary_data,
            1: self.parse_node_data,
            2: self.parse_element_data
        }

        while True:
            # We should only be back out here to parse a header or eof
            line = stream.readline()

            # Expect line == '' at eof, '\n' for empties
            if line == '':
                print "Warning: Found EOF before End of file card (99)."
                break

            # Skip empty lines
            if re.match('^\s*$', line):
                continue

            # Get the card header
            header = self.parse_header_card(line)

            # Check end of file flag
            if header["IT"] == 99:
                break

            # Get the parser if the header is recognized.
            # Else use the unknown card parser.
            parser = it_to_parser_dict.get(
                header["IT"], self.parse_unknown_card
            )

            # Parse the card
            parser(header, stream)


    @staticmethod
    def parse_header_card(line):
        r'''
        Identify the packet type from the packet header.

        Expect a line of the form (I2, 8I8)
            01   99 234 9283
        '''
        header_array = map(int, re.findall('\d+', line))
        return {
            "IT": header_array[0],
            "ID": header_array[1],
            "IV": header_array[2],
            "KC": header_array[3],
            "N" : header_array[4:]
        }


    #
    # Define some parsing functions.
    # We expect the first line has already been parsed
    # into header data
    #

    def parse_title_card(self, header, stream):
        r'''
        Read the Title Card (25).
        '''
        self._title = stream.readline()

        for _ in range(header["KC"]-1):
            stream.readline()


    def parse_summary_data(self, header, stream):
        r'''
        Read the Summary Data card (26).
        '''
        self._num_nodes              = header["N"][0]
        self._num_elements           = header["N"][1]
        self._num_materials          = header["N"][2]
        self._num_element_properties = header["N"][3]
        self._num_coordinate_frames  = header["N"][4]

        data_time_version = stream.readline()

        for _ in range(header["KC"] - 2):
            stream.readline()


    def parse_node_data(self, header, stream):
        r'''
        Read the Node Data card (01).
        '''

        # Ensure self._nodes exists and is the right size
        if self._nodes == None:
            if self._num_nodes == None:
                raise Exception(
                    "Error: Node Data (01) appears before Summary Data (26)."
                    + " Unable to parse file."
                )

            self._nodes = numpy.zeros(
                (self._num_nodes, 3), dtype=numpy.float64
            )

        node_id = header["ID"]
        node_coords_line = stream.readline()

        # Coords in form (3E16.9)
        coords = map(
            numpy.float64, re.findall('[+\-\.\deE]+', node_coords_line)
        )
        if len(coords) != 3:
            raise Exception(
                "Error while parsing Node Data (01), ID="
                + str(node_id) + ": "
                + "Failed to find 3 coordinate values for node.\n"
                + "Line: " + node_coords_line
            )

        # Node ids are 1 indexed. Need 0 indexed.
        self._nodes[node_id-1,:] = coords[:]

        # Data card 2, might be important, but we'll ignore it for now.
        extra_data_line = stream.readline()

        for _ in range(header["KC"]-2):
            stream.readline()


    def parse_element_data(self, header, stream):
        r'''
        Read the Element Data card (02).
        '''

        # Ensure self._elements exists and is the right size
        if self._elements == None or self._element_pids == None:
            if self._num_elements == None:
                raise Exception(
                    "Error: Element Data (02) appears before"
                    " Summary Data (26). Unable to parse file."
                )

            self._elements = numpy.zeros(
                (self._num_elements, 4), dtype=numpy.uintp
            )
            self._element_pids = numpy.zeros(
                self._num_elements, dtype=numpy.int
            )


        element_id = header["ID"]

        # Ensure tetrahedral element (IV=5)
        if header["IV"] != 5:
            raise Exception(
                "Error while parsing Element Data Card (02), ID="
                + str(element_id) + ": "
                + "Only Tetrahedral Element Data (IV=5) is supported.\n"
                + "Header: " + str(header)
            )

        config_line = stream.readline()
        config = re.search('^\s*(\d+)\s+(\d+)\s+(-?\d+)\s+(\d+)', config_line)
        if not config:
            raise Exception(
                "Error while parsing Element Data Card (02), ID="
                + str(element_id) + ": "
                + "Failed to parse Data Card 1.\n"
                + "Line: " + config_line
            )
        config = map(int, config.groups())

        # Ensure 4 nodes
        if config[0] != 4:
            raise Exception(
                "Error while parsing Element Data Card (02), ID="
                + str(element_id) + ": "
                + "Expecting NODES = 4 on Data Card 1.\n"
                + "Line: " + config_line
            )

        node_id_line = stream.readline()
        node_ids = map(numpy.uintp, re.findall('\d+', node_id_line))

        # node_ids 1 indexed. Need 0 indexed.
        node_ids = [node_id - numpy.uintp(1) for node_id in node_ids]

        if not node_ids:
            raise Exception(
                "Error while parsing Element Data Card (02), ID="
                + str(element_id) + ": "
                + "Failed to parse Data Card 2.\n"
                + "Line: " + node_id_line
            )

        if len(node_ids) < 4:
            raise Exception(
                "Error while parsing Element Data Card (02), ID="
                + str(element_id) + ": "
                + "Failed to find 4 node indices.\n"
                + "Line: " + node_id_line
            )


        # Set element node ids
        self._elements[element_id-1,:] = node_ids[:]
        # Set element property ids
        self._element_pids[element_id-1] = config[2]

        for _ in range(header["KC"] - 2):
            stream.readline()


    def parse_unknown_card(self, header, stream):
        print "Ignoring unsupported header IT: ", header["IT"]

        for _ in range(header["KC"]):
            stream.readline()
