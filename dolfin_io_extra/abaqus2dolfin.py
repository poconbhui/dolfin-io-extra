#!/usr/bin/env python2.6



from sys import argv, exit
from os.path import splitext
from dolfin_utils.meshconvert.meshconvert import convert2xml

import xml.etree.ElementTree as ET



def abaqus2dolfin(infile, material_id_map, outbase):
    """
    Convert input abaqus mesh file to a .raw.xml, .mesh.xml and .fn.xml file
    representing a dolfin xml version of the direct output of the
    convert2xml function, the mesh and the mesh function, where the
    mesh function takes the value based on the volume name.
    """

    raw_xml_filename     = outbase + ".raw.xml"
    mesh_xml_filename    = outbase + ".mesh.xml"
    mesh_fn_xml_filename = outbase + ".fn.xml"


    # Convert the output mesh to a mesh and mesh_fn file
    """
        Expect an xml document of the form:

        # slab.raw.xml:
        <dolfin xmlns:dolfin= >
            <mesh celltype= dim= >
                <vertices size= >
                    <vertex index= x= y= z= />
                    ...
                </vertices>
                <cells size= >
                    <tetrahedron index= v0= v1= v2= v3= />
                    ...
                </cells>

                <domains>
                    <mesh_value_collection name="ALLNODES" type= dim= size= >
                        <value cell_index= local_entity= value= />
                        ...
                    </mesh_value_collection>
                    <mesh_value_collection
                        name="mapped_space"
                        type=
                        dim=
                        size= >

                        <value cell_index= local_entity= value= />
                        ...
                    </mesh_value_collection>
                    <mesh_value_collection
                        name="unmapped_space"
                        type=
                        dim=
                        size= >

                        <value cell_index= local_entity= value= />
                        ...
                    </mesh_value_collection>
                    <mesh_value_collection name="magnetite" type= dim= size= >
                        <value cell_index= local_entity= value= />
                        ...
                    </mesh_value_collection>
                </domains>
            </mesh>
        </dolfin>


        We want two files:

        # slab.mesh.xml:
        <dolfin xmlns:dolfin= >
            <mesh celltype= dim= >
                <vertices size= >
                    <vertex index= x= y= z= />
                    ...
                </vertices>
                <cells size= >
                    <tetrahedron index= v0= v1= v2= v3= />
                    ...
                </cells>
            </mesh>
        </dolfin>

        # slab.fn.xml:
        <dolfin xmlns:dolfin= >
            <mesh_function>
                <mesh_value_collection name= type= dim= size= >
                    <value cell_index= local_entity= value= />
                    ...
                </mesh_value_collection>
            </mesh_function>
        </dolfin>


        In slab.fn.xml, the value of the value attribute in the value element
        should be 0 for magnetite, 1 for unmapped space and 2 for mapped space.

        In slab.raw.xml, <mesh_value_collection name="ALLNODES" ... > can be
        discarded. The values here are replicas of the other
        mesh_value_collection nodes, but doesn't contain a copy for
        every cell_index.
    """

    ###########################################################################
    # Generate outbase.raw.xml                                                #
    ###########################################################################
    #
    # Use the dolfin converter to generate the mesh xml file.
    #
    # The dolfin converter doesn't get the values in the
    # mesh_value_collection's right, so we'll fix that here as well.
    #


    # Convert the input mesh to a dolfin xml mesh
    convert2xml(infile, raw_xml_filename)


    # Get the raw converted mesh data
    raw_tree = ET.parse(raw_xml_filename)
    raw_root = raw_tree.getroot()
    raw_mesh = raw_root.find('mesh')
    raw_domains = raw_mesh.find('domains')
    raw_mvcs = raw_domains.findall('mesh_value_collection')

    # Remove any mesh_value_collection elements with names not in the
    # material_id_map
    for mvc in raw_mvcs:
        if not mvc.get('name') in material_id_map:
            raw_domains.remove(mvc)
            raw_mvcs.remove(mvc)

    # Set values in mesh_value_collections to the value in material_id_map
    # for the given mesh_value_collection name
    for mvc in raw_mvcs:
        mvc_name = mvc.get('name')
        material_id = material_id_map[mvc_name]

        for value in mvc:
            value.set('value', str(material_id))

    raw_tree.write(raw_xml_filename)


    ###########################################################################
    # Generate basename.mesh.xml                                              #
    ###########################################################################
    #
    # All needed data should be in the <mesh><vertices /><cells /></mesh>
    # elements of the raw mesh.
    #

    # Find .mesh.xml elements
    vertices = raw_mesh.find('vertices')
    cells    = raw_mesh.find('cells')

    # Make .mesh.xml from .raw.xml elements
    m_root = ET.XML('<dolfin></dolfin>')
    m_mesh = ET.Element('mesh', attrib=dict(raw_mesh.items()))
    m_root.append(m_mesh)
    m_mesh.append(vertices)
    m_mesh.append(cells)

    ET.ElementTree(m_root).write(mesh_xml_filename)


    ###########################################################################
    # Generate basename.fn.xml                                                #
    ###########################################################################
    #
    # All the needed data should be in the
    # <mesh><domains><mesh_value_collection /></domains></mesh> elements.
    #
    # All the values in the different raw mesh_value_collection elements will
    # be consolidated into one big mesh_value_collection with the name
    # "MaterialId"
    #

    # Make .fn.xml elements
    f_root = ET.XML(
        '<dolfin>' \
        +   '<mesh_function>' \
        +       '<mesh_value_collection />' \
        +   '</mesh_function>' \
        + '</dolfin>'
    )

    # Generate consolidated mesh_value_collection
    f_mvc = f_root.find('mesh_function').find('mesh_value_collection')

    # Add and count value entries from raw_mvcs
    f_mvc_size = 0
    for mvc in raw_mvcs:
        mvc_name = mvc.get('name')

        for value in mvc:
            f_mvc.append(value)
        f_mvc_size += int(mvc.get('size'))

    # Set attributes of the consolidated mesh_value_collection
    f_mvc_attrib = dict(raw_mvcs[0].items())
    f_mvc_attrib['size'] = str(f_mvc_size)
    f_mvc_attrib['name'] = "MaterialId"
    for attrib, value in f_mvc_attrib.iteritems():
        f_mvc.set(attrib, value)

    ET.ElementTree(f_root).write(mesh_fn_xml_filename)




if __name__ == "__main__":
    # Check correct usage
    if len(argv) < 4:
        print "Usage: python "+argv[0]+\
            " /path/to/mesh /path/to/material_id_map output_basename"
        print "    material_id_map should be a python file containing a"
        print "    dictionary called material_id_map from material names"
        print "    to integer ids."
        exit(1)

    # Get material_id_map
    import imp
    material_properties = imp.load_source("material_properties", argv[2])
    material_id_map = material_properties.material_id_map

    # Generate filenames
    infile  = argv[1]
    outbase = argv[3]


    # Run convert_abaqus
    abaqus2dolfin(infile, material_id_map, outbase)
