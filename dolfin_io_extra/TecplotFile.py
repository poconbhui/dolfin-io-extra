#!/usr/bin/env python

import dolfin
import re
import math


class TecplotFile:
    r'''
    A class representing a Tecplot file.

    Read a tecplot file:
        mesh = dolfin.Mesh()
        TecplotFile("file.dat") >> mesh

    Write a tecplot file:
        mesh = ...
        TecplotFile("file.dat") << mesh
    '''

    def __init__(self, filename, debug=False):
        self._filename = filename
        self._debug = debug

    def write(self, towrite):
        stream = open(self._filename, 'w')

        if isinstance(towrite, dolfin.Mesh):
            self.write_mesh_to_stream(towrite, stream)

    def __lshift__(self, towrite):
        self.write(towrite)

    def __rshift__(self, toread):
        self.read(toread)


    def read(self, toread, zone=0):
        stream = open(self._filename, 'r')
        zones = self.get_zones_from_stream(stream, debug=self._debug)
        zone = zones[zone]

        if isinstance(toread, dolfin.Mesh):
            self.zone_to_mesh(zone, dolfin_mesh = toread)

        if isinstance(toread, dolfin.Function):
            self.zone_to_function(zone, dolfin_function = toread)


    @staticmethod
    def write_mesh_to_stream(mesh, stream):

        # Write File Headers
        stream.write('TITLE = "%s"\n'%(mesh.name()))
        stream.write('FILETYPE = GRID\n')
        stream.write('VARIABLES = "X", "Y", "Z"\n')

        # Write Zone Headers
        stream.write(
            'ZONE T = "%s" N = %d E = %d F = FEPOINT ET = TETRAHEDRON\n'%(
                mesh.name(), mesh.num_vertices(), mesh.num_cells()
            )
        )

        # Write Zone Data
        for v in dolfin.vertices(mesh):
            stream.write('% .16e % .16e % .16e\n'%(v.x(0), v.x(1), v.x(2)))

        # Write Zone Footer
        for c in dolfin.cells(mesh):
            es = c.entities(0)
            # Tecplot is 1 indexed
            stream.write('%d %d %d %d\n'%(es[0]+1, es[1]+1, es[2]+1, es[3]+1))

    
    @staticmethod
    def zone_to_mesh(zone, dolfin_mesh):
        r'''
        Generate a dolfin mesh from zone data.

        If our input type was a mesh, add the vertices and tets found here.

        Use the MeshEditor class to create a mesh from the values stored in
        vertices and tetrahedra.

        The vertices list contains the coordinates for each vertex and
        the tetrahedra list contains lists of indices for its 4 vertices.
        '''

        # Create an editor for building the dolfin_mesh
        mesh_editor = dolfin.MeshEditor()
        mesh_editor.open(dolfin_mesh, 3,3)

        # Get the zone values we'll be using
        vertices = zone["vertices"]
        tetrahedra = zone["tetrahedra"]

        mesh_editor.init_vertices(len(vertices))
        mesh_editor.init_cells(len(tetrahedra))

        # Add the vertices
        for i, vertex in enumerate(vertices):
            mesh_editor.add_vertex(i, vertex[0], vertex[1], vertex[2])

        # Add the tetrahedra
        for i, tet in enumerate(tetrahedra):
            mesh_editor.add_cell(i, tet[0], tet[1], tet[2], tet[3])

        # Done!
        mesh_editor.close(True)


    @staticmethod
    def zone_to_function(zone, dolfin_function):
        r'''
        Generate a linear dolfin function from the read values
        and generated mesh.

        If the thing to read was a dolfin function, add the values found
        here to the function.

        The vertex values of the function are given in the values array.

        Setting vertex indices in dolfin is just a little awkward. Ideally,
        we could do something like fn(x,y,z) = value. Instead, we have to
        poke around at the underlying vector representation.

        Since the indices of the vector representation don't necessarily
        correlate with the indices of the vertices, we have to use
        a dofmap to correlate them.
        '''

        # Get the zone values
        values = zone["values"]
        values_width = len(values[0])


        # Check the function space is CG 1, dim=values_width
        V = dolfin_function.function_space()
        ufl_element = V.ufl_element()

        if ufl_element.degree() != 1 or ufl_element.family() != 'Lagrange':
            raise ValueError(
                "Error: Input Function must Lagrange, Degree 1."
            )
        if ufl_element.value_shape() != (values_width,):
            raise ValueError(
                "Error: Input Function must have value shape of (%d,)."%(
                    values_width
                )
            )
                


        # Get the dofs of the function and a map from dofs to
        # mesh vertices.
        # (Fairly hacky and ugly. Seems to work for now (in serial).
        function_vector = dolfin_function.vector()
        dof_to_v = dolfin.dof_to_vertex_map(V)

        # Get dof indices for each dimension separately
        xs_dofs = [V.sub(i).dofmap().dofs() for i in range(values_width)]

        # Loop over the dofs
        for xs_dof in zip(*xs_dofs):

            # Dofs offset by 3 because the function holds a 3d vector
            # at each vertex.
            #
            # The documentation for this is a little hairy, so I *think*
            # this is right...
            dof3d_to_v = lambda dof: int(math.floor(dof_to_v[dof]/len(xs_dof)))

            # Set the function_vector, and so, the function
            for i, xi_dof in enumerate(xs_dof):
                function_vector[xi_dof] = values[dof3d_to_v(xi_dof)][i]



    @staticmethod
    def get_zones_from_stream(stream, debug=False):
        r'''
        Given an input TECPLOT file, return a dolfin mesh and linear function.
        '''

        parser = Parser()
        # Iterate over the lines in the file
        for n_line, line in enumerate(stream):
            if not parser.parse(line):
                break

        if debug == True:
            # Look at the first zone only
            zone = parser.zones()[izone]
            headers    = zone["headers"]
            vertices   = zone["vertices"]
            values     = zone["values"]
            tetrahedra = zone["tetrahedra"]

            print
            print "---------------------------"
            print "     Tecplot File Info     "
            print "---------------------------"
            print
            print "title = '%s'"%(headers["Title"])
            print "variables = ", headers["Variables"]
            print "T  = '%s'"%(headers["T"])
            print "N  = ", headers["N"]
            print "E  = ", headers["E"]
            print "F  = ", headers["F"]
            print "ET = ", headers["ET"]
            print
            print "Vertices Found:   ", len(vertices)
            print "Values Found:     ", len(values)
            print "Tetrahedra found: ", len(tetrahedra)
            print


        return parser.zones()



#######################################################################
# Tecplot Parsers                                                     #
#######################################################################
#
# Reading the list file and parse it into dolfin xml mesh and
# linear function files.
#
# Assuming the format:
#   TITLE = "..."
#   VARIABLES = "...","...",...
#   ZONE T = "..." N = ... , E = ...
#   F = ... , ET = ...
#   x y z v1 v2 v3
#   x y z v1 v2 v3
#   ...
#   t1 t2 t3 t4
#   t1 t2 t3 t4
#
# where
#   N is the number of nodes (ie x,y,z values)
#   E is the number of tetrahedra (ie t1,t2,t3,t4 values)
#
# The tetrahedra vertices are given by the node indices with the
# node indices as the 1 indexed position in the x,y,x,v1,v2,v3 list
#


class Headers():
    r'''
    A headers class representing Tecplot headers.
    '''
    def __init__(self):
        self.title = ""
        self.variables = []
        self.T = ""
        self.N = 0
        self.E = 0
        self.F = ""
        self.ET = ""


class ZoneHeaderParser:
    r'''
    A parser for parsing Tecplot Zone headers.

    The parse method should be called on each line.
    It will return true if it managed to parse a header value, or
    false otherwise.

    Expect data in the form
    TITLE = "..."
     FILETYPE = FULL
    VARIABLES = "V1", "V2", "V3"
       ZONE T="..." E=...
    F=...
    ET=...
    
    ie any initial spacing, generally one line each, but ZONE data can
    spill over several lines
    '''

    def __init__(self):
        self._headers = {
            "Filetype": "FULL",
            "Variables": ["X", "Y", "Z"]
        }

    def headers(self):
        return self._headers

    def __getitem__(self, key):
        return self.headers()[key]

    def __setitem__(self, key, value):
        self.headers()[key] = value


    def zone_start(self, line):
        r'''
        Return true if the line marks the start of a new zone header.
        '''
        match = re.search('^ *ZONE')
        if match:
            return True
        else:
            return False

    #
    # Define header parsing functions
    #

    def parse_title(self, line):
        match = re.search('^ *TITLE *= *"([\w ]*?)" *$', line)
        if match:
            return match.group(1)

    def parse_filetype(self, line):
        match = re.search('^ *FILETYPE *= *(\w+) *$', line)
        if match:
            return match.group(1)

    def parse_variables(self, line):
        match = re.search('^ *VARIABLES *= *("\w+" *,? *)*$', line)
        if match:
            return re.findall('"(\w+?)"', line)

    def parse_T(self, line):
        match = re.search('T *= *"(.*?)"', line)
        if match:
            return match.group(1)

    def parse_N(self, line):
        match = re.search('N *= *(\d+)', line)
        if match:
            return int(match.group(1))

    def parse_E(self, line):
        match = re.search('E *= *(\d+)', line)
        if match:
            return int(match.group(1))

    def parse_F(self, line):
        match_F = re.search('F *= *(\w+)', line)
        match_DATAPACKING = re.search('F *= *(\w+)', line)
        if match_F:
            return match_F.group(1)
        if match_DATAPACKING:
            return match_DATAPACKING.group(1)

    def parse_ET(self, line):
        match_ET = re.search('ET *= *(\w+)', line)
        match_ZONETYPE = re.search('ZONETYPE *= *(\w+)', line)
        if match_ET:
            return match_ET.group(1)
        if match_ZONETYPE:
            return match_ZONETYPE.group(1)


    def parse(self, line):
        r'''
        Parse a line using all parser functions.
        On success, return true.
        When all headers have been parsed, always return false.
        '''

        # Ignore empty lines and comments
        if re.search('^ *$', line):
            return True
        if re.search('^ *#', line):
            return True

        # List header keys and parsing functions
        header_parsers = [
            ("Title", self.parse_title),
            ("Filetype", self.parse_filetype) ,
            ("Variables", self.parse_variables),
            ("T", self.parse_T),
            ("N", self.parse_N),
            ("E", self.parse_E),
            ("F", self.parse_F),
            ("ET", self.parse_ET)
        ]


        # Try all header parsing functions.
        #
        # If any succeed, we're still in the headers. Move on to the
        # next line and continue parsing.
        #
        # If they all fail, we're no longer in the headers!
        # Move on to processing the vertices from this line on.
        #
        any_parsed = False
        for key, parser in header_parsers:
            value = parser(line)
            if value is not None:
                self[key] = value
                any_parsed = True
                
        return any_parsed


    def headers_ok(self):
        r'''
        Do sanity check on the headers.
        '''

        if not self["Filetype"] == "FULL":
            raise Exception("FILETYPE must be GRID or FULL")
        if not self["N"] > 0:
            raise Exception("Must specify the number of vertices N!")
        if not self["E"] > 0:
            raise Exception("Must specify number of elements E!")
        if not self["F"] == "FEPOINT":
            raise Exception("F or DATAPACKING must be FEPOINTS!")
        if not self["ET"] == "TETRAHEDRON":
            raise Exception("ET or ZONETYPE must be TETRAHEDRON!")


class FEPointZoneDataParser:
    r'''
    A parser for parsing Tecplot FEPOINT Zone Data,
    ie a list of vertices and values.

    The parse method should be called on each line after the headers.
    The parser will read num_vertices lines, parsing the vertices and
    values and returning true.
    After num_vertices lines, it will assume all the vertices and values
    have been read and always return false.
    A new parser should be used for every Zone data to parse.

    Expect data in the form
        X Y Z V1 V2 ...
        X Y Z V1 V2 ...
    which is one vertex/value group per line

    With numbers of the form
        1.000E+93 -2.9982E-98 498324E88
    which is numbers with decimals and exponents and +/-, separated by
    wither a white space or a comma.

    Repeated values (3*8.8383E84) are not supported.
    '''

    def __init__(self, zone_headers):
        if not zone_headers["F"] == "FEPOINT":
            raise NotImplementedError(
                "Only FEPOINT data is implemented in FEPointZoneDataParser"
            )

        if not len(zone_headers["Variables"]) >= 3:
            raise Exception("Must have at least 3 variables for X,Y,Z")
        if not zone_headers["Variables"][0] == "X":
            raise Exception("First VARIABLE must be X.")
        if not zone_headers["Variables"][1] == "Y":
            raise Exception("Second VARIABLE must be Y.")
        if not zone_headers["Variables"][2] == "Z":
            raise Exception("Third VARIABLE must be Z")


        self._num_vertices = zone_headers["N"]

        self._vertices = []
        self._values = []

    def vertices(self):
        return self._vertices

    def values(self):
        return self._values

    def is_done(self):
        return len(self.vertices()) >= self._num_vertices

    def parse(self, line):
        # Ignore empty lines and comments
        if re.match('^ *$', line):
            return True
        if re.match('^ *#', line):
            return True

        # Match line of floats while the current total is less than the
        # expected maximum
        if not self.is_done():
            matches = re.findall('[+\-.\deE]+', line)
            self.vertices().append([float(v) for v in matches[0:3]])
            self.values().append([float(v) for v in matches[3:]])
            return True
        else:
            return False


class TetrahedronZoneFooterParser:
    r'''
    A parser for Tecplot TETRAHEDRON Zone Footers.
    This is only implemented for TETRAHEDRON Zonetypes, and will contain
    the tetrahedron connectivities.

    This should be called on every line after the vertices.
    This reads the tetrahedra and returns true.
    After reading num_tetrahedra lines, it will assume all the tetrahedra
    have been read and always return false.
    A new parser should be made for every Zone footer to parse.
    '''

    def __init__(self, zone_headers):
        if not zone_headers["ET"] == "TETRAHEDRON":
            raise NotImplementedError(
                "Only ET = TETRAHEDRON implemented for "
                "TetrahedronZoneFooterParser"
            )

        self._num_tetrahedra = zone_headers["E"]
        self._tetrahedra = []

    def tetrahedra(self):
        return self._tetrahedra

    def is_done(self):
        return len(self.tetrahedra()) >= self._num_tetrahedra

    def parse(self, line):
        # Ignore empty lines and comments
        if re.match('^ *$', line):
            return True
        if re.match('^ *#', line):
            return True

        if not self.is_done():
            matches = re.findall('\d+', line)

            # Tecplot file is 1 indexed. We want 0 indexed.
            self.tetrahedra().append([int(t)-1 for t in matches])

            return True
        else:
            return False


class Parser:
    r'''
    A Tecplot file parser.

    This parser calls the HeaderParser, VertexParser and TetrahedronParser
    in an appropriate order.

    This should be called on each line of the file, and includes comment
    handling.
    '''

    def __init__(self):

        self.PARSING_ZONE_HEADERS = 1
        self.PARSING_ZONE_DATA    = 2
        self.PARSING_ZONE_FOOTER  = 3
        self.PARSING_DONE         = 4

        # A list containing all the parseers and parsed data
        self._zones = []

        # We should always have at least 1 zone, and should start
        # parsing headers
        self._zones.append({"state": self.PARSING_ZONE_HEADERS})


    def zones(self):
        return self._zones


    def parse(self, line):

        # Ignore empty lines and comments
        if re.match('^ *$', line):
            return True
        if re.match('^ *#', line):
            return True

        # We expect Zones to be in the form
        #   Zone 1 Header
        #   Zone 1 Data
        #   Zone 1 Footer
        #   Zone 2 Header
        #   Zone 2 Data
        #   ...

        # Get the last zone
        zone = self.zones()[-1]


        # Parse Zone headers
        if zone["state"] == self.PARSING_ZONE_HEADERS:

            if "zone_header_parser" not in zone:
                zone["zone_header_parser"] = ZoneHeaderParser()

            parser = zone["zone_header_parser"]

            # Try parsing headers and zone headers
            if parser.parse(line):
                return True

            # If headers not parsed, we're now reading Zone headers
            zone["headers"] = zone["zone_header_parser"].headers()

            # Start reading Zone data
            zone["state"]   = self.PARSING_ZONE_DATA


        # Parse zone data
        if zone["state"] == self.PARSING_ZONE_DATA:

            if "zone_data_parser" not in zone:
                if zone["headers"]["F"] == "FEPOINT":
                    zone["zone_data_parser"] = FEPointZoneDataParser(
                        zone["headers"]
                    )
                else:
                    raise NotImplementedError(
                        "Only FEPOINT Zone data is currently supported."
                    )

            parser = zone["zone_data_parser"]

            # Try parsing Zone data
            parsed_ok = parser.parse(line)

            # If the parser is done, move on to parsing zone footers
            if parser.is_done():
                zone["vertices"] = zone["zone_data_parser"].vertices()
                zone["values"]   = zone["zone_data_parser"].values()

                # Start reading Zone footers
                zone["state"] = self.PARSING_ZONE_FOOTER


            # If something was parsed, return early
            if parsed_ok:
                return True


        # Parse Zone footers
        if zone["state"] == self.PARSING_ZONE_FOOTER:

            if "zone_footer_parser" not in zone:
                if zone["headers"]["ET"] == "TETRAHEDRON":
                    zone["zone_footer_parser"] = TetrahedronZoneFooterParser(
                        zone["headers"]
                    )
                else:
                    raise NotImplementedError(
                        "Only TETRAHEDRON Zone Footer is currently supported."
                    )

            parser = zone["zone_footer_parser"]

            # Try parsing Zone footer
            parsed_ok = parser.parse(line)

            if parser.is_done():
                zone["tetrahedra"] = zone["zone_footer_parser"].tetrahedra()

                # This Zone should be fully parsed
                zone["state"] = self.PARSING_DONE

            if parsed_ok:
                return True


        # After the footer, we want to check if we should be parsing a
        # new Zone header.
        if zone["zone_header_parser"].zone_start(line):
            self.zones().append({
                "state" : self.PARSING_ZONE_HEADERS
            })

            # Try parsing this line again with the new zone added
            return self.parse(line)


        # If we got here... uh oh.
        return False
